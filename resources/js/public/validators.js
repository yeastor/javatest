/**
 * Created by KupriyanovYM on 14.12.2016.
 */
define(['jquery'],function($){



    $.fn.extend({

        validate : function(options) {
            var obj = $(this),
                valid = true,
                params;
                validateReq = function (element,value) {
                    if (element.attr('type') == 'checkbox'){
                        return element.is(':checked');
                    }

                    return value.length > 0;
                },
                validateEmail = function (element,value) {
                    if (!validateReq(element,value)) return true;
                    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value));
                },
                validatePhone = function (element,value) {
                    if (!validateReq(element,value)) return true;
                    return (/^([\d]{7,14})$/.test(value));
                },
                validationEqual = function (element,value) {
                    var name = element.attr('name');
                    if (params[name].passId === undefined){
                        Console.log('validation params not set');
                        return false;
                    }
                    return $(document.getElementById(params[name].passId)).val() == value;
                },
                validationRemote = function (element,value) {
                   var name = element.attr('name'),
                       getdata = {},
                       valid = false;
                    if (!validateReq(element,value)) return true;
                    if (params[name].url === undefined || params[name].value === undefined){
                        Console.log('validation params not set');
                        return false;
                    }
                        getdata[params[name].value] = value;

                    $.ajax({
                            url: params[name].url,
                            data: getdata,
                            async: false
                        })
                        .done(function(data) {
                            valid = data.indexOf("ok") !== -1;
                        })
                        .fail(function(res) {
                            valid = false;
                        });

                    return valid;

                },


            validatorsClass =
            { 'required' : validateReq,
                'email' : validateEmail,
                'phone' : validatePhone,
                'remote' : validationRemote,
                'equal' : validationEqual
            };


            params = options;

            obj.find('input').each(function( index, element ){
                elementIsValid = true;
                $.each(validatorsClass,
                    function( index, callback ) {
                        if ($( element ).hasClass(index)) {
                            container = $( element ).parent('div');
                            if (!callback($( element ),$( element ).val())) {
                                valid = false;
                                elementIsValid = false;
                                container.addClass('has-error');
                                container.find('.message-'+index).removeClass('hidden');
                            } else {
                              if (elementIsValid) {
                                container.removeClass('has-error');
                                container.find('.message-'+index).addClass('hidden');
                                }

                            }

                        }
                    });



            });

            return valid;
        }


    });
    return{

    }
});
