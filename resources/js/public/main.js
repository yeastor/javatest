require.config({
    baseUrl: "resources/js/public",
    paths: {
        jquery: '../libs/jquery',
        bootstrap: '../libs/bootstrap/dist/js/bootstrap.min',
        datepicker: '../libs/datepicker/js/bootstrap-datepicker'
    },
    shim: {
        datepicker: ['jquery'],
        'bootstrap': ['jquery']

    }
});

