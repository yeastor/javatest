/**
 * Created by KupriyanovYM on 14.12.2016.
 */
define(['regendform/view'],

    function(RegEndFormView) {

        var RegEndFormController = function () {
            this._view = new RegEndFormView('reg-end-from');
            this._onSubmited = function (result) {
                //TODO отдаем на сервер;
                window.location = 'index2.html';
            };


            this._arratchHandlers = function(){
                this._view.events.on('formSubmited', $.proxy(this._onSubmited,this));
            };

            this._arratchHandlers();
        };

        RegEndFormController.prototype = {
            constructor: RegEndFormController,
            getView: function () {
                return null;
            }
        };

        return RegEndFormController;


    });