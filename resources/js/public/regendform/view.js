/**
 * Created by KupriyanovYM on 14.12.2016.
 */
define(['jquery','bootstrap','datepicker','validators','pass-checker'],

    function($,Bootstrap,Datepicker) {

        var RegEndFormView = function (formId) {
            this._form = $(document.getElementById(formId));
            this._pass = this._form.find("#passw");
            this._passAgain = this._form.find("#passwd-again");
            this.events = $({});
            this._validation = {
                login: {
                    url: 'http://getbootstrap.com/',//'http://localhost:8080/iFiles/validation.jsp?mode=login',
                    value: 'val'
                },
                'passwd-again' : {
                    passId: 'passw'
                }

            };
            this._arratchHandlers = function(){
                this._form.on('submit', $.proxy(this._onSubmit,this));
                this._pass.on('keyup', $.proxy(this._checkPass,this));
                this._passAgain.on('keyup', $.proxy(this._checkPass,this));
            };

            this._onSubmit = function (e) {
                e.preventDefault();
                if (this._form.validate(this._validation)) {
                    this.events.trigger($.Event('formSubmited',{formData: this._form.serialize()}));
                }
            };

            this._checkPass = function (e) {
                e.preventDefault();
                if (this._pass.val() == this._passAgain.val()){
                    $('.js-check-equal .glyphicon-ok').removeClass('hidden');
                    $('.js-check-equal .glyphicon-remove').addClass('hidden');
                }
                else {
                    $('.js-check-equal .glyphicon-remove').removeClass('hidden');
                    $('.js-check-equal .glyphicon-ok').addClass('hidden');
                }
            };

            this._pass.passcheker({class: 'progress'});
                this._arratchHandlers();

        };

        RegEndFormView.prototype = {
            constructor: RegEndFormView,
            getView: function () {
                return null;
            }

        };

        return RegEndFormView;


    });