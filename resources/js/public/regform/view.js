/**
 * Created by KupriyanovYM on 14.12.2016.
 */
/**
 * Created by KupriyanovYM on 14.12.2016.
 */
define(['jquery','bootstrap','datepicker','validators'],

    function($,Bootstrap,Datepicker) {

        var RegFormView = function (formId) {
            this._form = $(document.getElementById(formId));
            this._validation = {
                email: {
                    url: 'http://getbootstrap.com/getting-started/',//'http://localhost:8080/iFiles/validation.jsp?mode=email',
                    value: 'val'
                }
            };
            this._datepicker = this._form.find('#datepicker3');
            this._birthday = this._form.find('#birthday');
            this._certificateType = this._form.find('#dropdownMenu5');
            this._certificateTypeInput =  this._form.find('#certificate_type');
            this._certificateTypeItems =  this._form.find('[aria-labelledby="'+this._certificateType.attr('id')+'"] li a');
            this.events = $({});

            this._createDatePicker = function () {
                this._datepicker.data('date','01.01.1985');

            };

            this._arratchHandlers = function(){
                this._datepicker.datepicker().on('changeDate', $.proxy(this._onDpChange,this));
                this._certificateTypeItems.on('click', $.proxy(this._onCertItemClick,this));
                this._form.on('submit', $.proxy(this._onSubmit,this));
            };

            this._onDpChange = function () {
                this._datepicker.datepicker('hide');
                this._birthday.val(this._datepicker.data('date'));
            };

            this._onCertItemClick = function (ev) {
                this._certificateType.find('#certificate_type_text').text(ev.currentTarget.innerText);
                this._certificateTypeInput.val(ev.currentTarget.id);
            };

            this._onSubmit = function (e) {
                e.preventDefault();
                if (this._form.validate(this._validation)) {
                    this.events.trigger($.Event('formSubmited',{formData: this._form.serialize()}));
                }
            };
                this._createDatePicker();
                this._arratchHandlers();

        };

        RegFormView.prototype = {
            constructor: RegFormView,
            getView: function () {
                return null;
            }

        };

        return RegFormView;


    });