/**
 * Created by KupriyanovYM on 14.12.2016.
 */
define(['regform/view'],

    function(RegFormView) {

        var RegFormController = function () {
            this._view = new RegFormView('reg-from');
            this._onSubmited = function (result) {
                //TODO отдаем на сервер result.formData или сохраняем в куках;
                window.location = 'index2.html';
            };


            this._arratchHandlers = function(){
                this._view.events.on('formSubmited', $.proxy(this._onSubmited,this));
            };

            this._arratchHandlers();
        };

        RegFormController.prototype = {
            constructor: RegFormController,
            getView: function () {
                return null;
            }
        };

        return RegFormController;


    });