/**
 * Created by KupriyanovYM on 14.12.2016.
 */
define(['jquery'],function($){

    $this = this;

    $.fn.extend({

        passcheker : function(option) {
            var obj = $(this),
                valid = true,
                params;

            this.bar = $('<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">'+
                '<span class="sr-only"></span></div>');
            $(document.getElementsByClassName(option.class)).append(this.bar);
            this.check = function (ev) {
                var score,
                   pbClass;
                score = this.scorePassword(ev.currentTarget.value);
                pbClass = this.getPbClass(score);



                this.bar.css('width',score+'%');
                this.bar.removeClass( 'progress-bar-success');
                this.bar.removeClass( 'progress-bar-warning');
                this.bar.removeClass( 'progress-bar-danger');
                this.bar.addClass( 'progress-bar-'+pbClass);

            };
            this.getPbClass = function(score){
                if (score > 80)
                    return 'success';
                if (score > 60)
                    return  'warning';

                    return  'danger';
            };
            this.scorePassword =function(pass) {
                var score = 0;
                if (!pass)
                    return score;

                // award every unique letter until 5 repetitions
                var letters = new Object();
                for (var i=0; i<pass.length; i++) {
                    letters[pass[i]] = (letters[pass[i]] || 0) + 1;
                    score += 5.0 / letters[pass[i]];
                }

                // bonus points for mixing it up
                var variations = {
                    digits: /\d/.test(pass),
                    lower: /[a-z]/.test(pass),
                    upper: /[A-Z]/.test(pass),
                    nonWords: /\W/.test(pass),
                }

                variationCount = 0;
                for (var check in variations) {
                    variationCount += (variations[check] == true) ? 1 : 0;
                }
                score += (variationCount - 1) * 10;

                return parseInt(score);
            },

            obj.on('keyup', $.proxy(this.check,this));
            return valid;
        }


    });
    return{

    }
});
